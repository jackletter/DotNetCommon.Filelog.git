﻿using System;

namespace DotNetCommon.Filelog
{
    public class NullScope : IDisposable
    {
        private static readonly NullScope _instance = new NullScope();
        private NullScope()
        {
        }

        public void Dispose()
        {
        }

        public static NullScope Instance => _instance;
    }
}
