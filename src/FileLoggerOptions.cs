﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace DotNetCommon.Filelog
{
    /// <summary>
    /// 日志输出配置选项
    /// </summary>
    public class FileLoggerOptions
    {
        /// <summary>
        /// 当配置相对目录时，使用的基目录地址，默认: AppDomain.CurrentDomain.BaseDirectory
        /// </summary>
        public string BaseDirectory { get; set; }

        /// <summary>
        /// 是否包含Scope
        /// </summary>
        public bool? IncludeScopes { set; get; }

        /// <summary>
        /// FileLog内部的日志的输出位置,格式同Writer的Path
        /// </summary>
        public string InternalLogFile { set; get; }

        /// <summary>
        /// 日志目录净化功能
        /// </summary>
        public AutoClear AutoClear { get; set; }

        /// <summary>
        /// 一系列的日志输出文件
        /// </summary>
        public List<Writer> Writers { set; get; }

        /// <summary>
        /// 是否开启默认日志输出
        /// </summary>
        public bool EnableDefaultWriters { get; set; } = true;
    }

    /// <summary>
    /// 日志输出文件位置
    /// </summary>
    public class Writer
    {
        /// <summary>
        /// 日志的文件输出路径,可以是相对路径或绝对路径,可以嵌入时间格式化传,如: logs/system-info-#datetime{yyyy-MM-dd}#.log
        /// </summary>
        public string Path { set; get; }

        /// <summary>
        /// 首先包含的日志记录类别数组
        /// </summary>
        public List<string> FirstInclude { set; get; }

        /// <summary>
        /// 从包含的日志记录类别中排除类别数组
        /// </summary>
        public List<string> SecondExclude { set; get; }

        /// <summary>
        /// 文件滚动大小,单位字节,默认10M=1024*1024*10
        /// </summary>
        public long RollingSize { get; set; } = 1024 * 1024 * 10;

        /// <summary>
        /// 最低日志记录级别，默认：Trace
        /// </summary>
        public LogLevel MinLogLevel { set; get; } = LogLevel.Trace;

        /// <summary>
        /// 最高日志记录级别，默认：None
        /// </summary>
        public LogLevel MaxLogLevel { set; get; } = LogLevel.None;
    }

    /// <summary>
    /// 自动净化日志目录功能
    /// </summary>
    public class AutoClear
    {
        /// <summary>
        /// 是否开启日志目录净化功能
        /// </summary>
        public bool? Enable { get; set; }

        /// <summary>
        /// 过期净化时间，单位秒,默认0,即不净化
        /// </summary>
        public long? ExpireSpan { get; set; }

        /// <summary>
        /// 触发目录净化的条件,默认200,即: 每当FileLog执行了200次输出就会触发目录净化
        /// </summary>
        public int? TriggerCount { get; set; }

        /// <summary>
        /// 需要净化的目录列表,净化时采用的是递归算法
        /// </summary>
        public List<string> Dirs { get; set; }

        /// <summary>
        /// 为防止误删除文件,必须设置日志文件的后缀名
        /// </summary>
        public List<string> Exts { get; set; }
    }
}
