﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Filelog
{
    public class PostConfigureFileLoggerOptions : IPostConfigureOptions<FileLoggerOptions>
    {
        public void PostConfigure(string name, FileLoggerOptions options)
        {
            //BaseDirectory：默认为AppDomain.CurrentDomain.BaseDirectory
            if (string.IsNullOrWhiteSpace(options.BaseDirectory)) options.BaseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            //IncludeScopes：默认为true
            if (options.IncludeScopes == null) options.IncludeScopes = true;
            //InternalLogFile: 默认"logs/internal-#datetime{yyyy-MM-dd}#.log"
            if (string.IsNullOrWhiteSpace(options.InternalLogFile)) options.InternalLogFile = "logs/internal-#datetime{yyyy-MM-dd}#.log";
            //AutoClear
            if (options.AutoClear == null) options.AutoClear = new AutoClear();
            if (options.AutoClear.Enable == null) options.AutoClear.Enable = false;
            if (options.AutoClear.ExpireSpan == null) options.AutoClear.ExpireSpan = 0;
            if (options.AutoClear.TriggerCount == null) options.AutoClear.TriggerCount = 200;
            if (options.AutoClear.Dirs == null) options.AutoClear.Dirs = new List<string>() { options.BaseDirectory };
            if (options.AutoClear.Exts == null) options.AutoClear.Exts = new List<string>() { "*.log" };
            //Writers
            if (options.Writers == null) options.Writers = new List<Writer>();
            //EnableDefaultWriters
            if (options.EnableDefaultWriters)
            {
                options.Writers.Add(new Writer()
                {
                    FirstInclude = new List<string>() { "Microsoft", "System" },
                    Path = "logs/system/system-#datetime{yyyy-MM-dd}#.log",
                    RollingSize = 1024 * 1024 * 10L
                });
                options.Writers.Add(new Writer()
                {
                    FirstInclude = new List<string>() { "*" },
                    Path = "logs/app-#level#-#datetime{yyyy-MM-dd}#.log",
                    SecondExclude = new List<string>() { "Microsoft", "System" },
                    RollingSize = 1024 * 1024 * 10L
                });
                options.Writers.Add(new Writer()
                {
                    FirstInclude = new List<string>() { "*" },
                    Path = "logs/app-all-#datetime{yyyy-MM-dd}#.log",
                    SecondExclude = new List<string>() { "Microsoft", "System" },
                    RollingSize = 1024 * 1024 * 10L
                });
            };
        }
    }
}
