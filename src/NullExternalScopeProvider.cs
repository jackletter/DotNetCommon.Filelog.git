﻿using Microsoft.Extensions.Logging;
using System;

namespace DotNetCommon.Filelog
{
    public class NullExternalScopeProvider : IExternalScopeProvider
    {
        private static readonly IExternalScopeProvider _instance = new NullExternalScopeProvider();

        void IExternalScopeProvider.ForEachScope<TState>(Action<object, TState> callback, TState state)
        {

        }

        IDisposable IExternalScopeProvider.Push(object state)
        {
            return NullScope.Instance;
        }

        public static IExternalScopeProvider Instance =>
           _instance;
    }
}
