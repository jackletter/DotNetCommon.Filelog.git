﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleFileLog.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public string Get()
        {
            _logger.LogTrace("trace");
            _logger.LogDebug("debug");
            _logger.LogInformation("info");
            _logger.LogWarning("warn");
            _logger.LogError("error");
            _logger.LogCritical("critical");
            using (_logger.BeginScope("测试日志的Scope"))
            {
                _logger.LogTrace("scope-trace");
                _logger.LogDebug("scope-debug");
                _logger.LogInformation("scope-info");
                _logger.LogWarning("scope-warn");
                _logger.LogError("scope-error");
                _logger.LogCritical("scope-critical");
            }
            _logger.LogTrace("Scope已经结束了。。。");
            //多行日志
            _logger.LogInformation($@"第几次
小明小红
小花小龙圣诞节哦我按实际大地阿斯顿");
            _logger.LogInformation($@"hah 
小明小红
小花小龙圣诞节哦我按实际大地阿斯顿end");
            //throw new Exception("测试异常日志");
            return "ok";
        }
    }
}
